variable "aws_region" {
  default = "ap-southeast-1"
}

variable "project_code" {
  default = "vpcsg"
}
